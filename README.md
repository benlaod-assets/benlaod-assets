## Setting the Scoped Registry in Unity

When in  `Project Settings > Package Manager`, add a Scoped Registry :
- Name : `Benlaod Assets` : 
- URL : `https://gitlab.com/api/v4/projects/31465142/packages/npm`
- Scope(s) : 
    - `fr.benjaminbrasseur`

Then click on `Apply`.
Now you can pull the packages from the list bellow !

## List of current assets
- [Dialogue System](https://gitlab.com/benlaod-assets/dialogue-system)
  - [Simple Dialogue Nodes](https://gitlab.com/benlaod-assets/simple-dialogue-nodes)
- [Game Event](https://gitlab.com/benlaod-assets/game-event)
- [Game Variables](https://gitlab.com/benlaod-assets/game-variables)
- [Parameter Manager](https://gitlab.com/benlaod-assets/parameter-manager)
